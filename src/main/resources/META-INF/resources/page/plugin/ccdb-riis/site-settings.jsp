<%@ page session="true" contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pg" tagdir="/WEB-INF/tags/page" %>

<c:set var="redirect">
    <div class="error">Not authorized. Redirecting...</div>
    <script>
        window.setTimeout(function(){
            window.location.href = '<c:url value="/"/>'
        }, 2000);
    </script>
</c:set>

<pg:restricted msg="${redirect}">

    <style type="text/css">

        .riisHidden {
            display: none;
        }

        .riisError {
            color: red;
        }

        #riis_mgmt_div {
            width: 50%;
            float: left;
            box-sizing: border-box;
        }

        #riis_user_table table {
            width: 100%;
            margin-top: 10px;
            margin-bottom: 30px;
        }

        #riis_user_table table .yui-dt-col-removeUser {
            text-align: center;
        }

        #riis_add_div {
            width: 50%;
            float: right;
            box-sizing: border-box;
        }

        #riis_add_div > .pad {
            padding-left: 30px;
        }

        #riis_threshold_display {
            /*padding-top: 5px;*/
            /*padding-left: 2px;*/
            font-weight: bold;
        }

        .riis_critical_error {
            text-align: center;
            width: 100%;
            padding-left: 0px;
            padding-right: 0px;
            margin-top: 5px;
            margin-bottom: 2px;
        }

        #riisUser, #riisThreshold {
            margin-bottom: 3px;
        }

        #riis_login_records {
            width: 100%;
        }

        #riis_login_records table {
            width: 100%;
        }

        .riis_header {
            font-weight: bold;
        }

    </style>

    <c:url var="SCRIPTS" value="/scripts"/>

    <!-- title: RIIS -->
    <script type="text/javascript" src="${SCRIPTS}/xnat/plugin/ccdb-riis/riisConfigMgmt.js"></script>

    <div id="riis_critical_err_holder" class="riisHidden"></div>

    <div id="mgmt_container">

        <!-- float left -->
        <div id="riis_mgmt_div">
            <header class="riis_header">Configured Users</header>
            <!-- table listing configured users -->
        </div>

        <!-- float right -->
        <div id="riis_add_div"><div class="pad">

            <header class="riis_header">Add Riis User</header>
            <div>Add users that are allowed to post to the riis service.</div>
            <br>
            <input type="text" name="riisUser" id="riisUser">
            <button type="button" id="add_riis_user_btn" class="btn btn-sm">Add</button>
            <br>
            <div id="userAddError" class="riisHidden riisError"></div>

            <hr>

            <header class="riis_header">Set the Riis Threshold</header>
            <div>Enter the threshold value. (minutes)</div>
            <br>
            <input type="text" name="riisThreshold" id="riisThreshold" size="3" maxlength="4">
            <button type="button" id="set_riis_threshold" class="btn btn-sm">Set</button>
            <br>
            <div id="thresholdError" class="riisHidden riisError"></div>

        </div></div>
            </div>

    <br class="clear">

    <hr>

    <div id="riis-records-container">

        <button type="button" id="view_login_records" class="btn btn-sm">Show/Hide Recent Login Records</button>
        <div class="pad10v"></div>
    <div id="riis_login_records"></div>

    </div>

    <script>

        (function(){

            function addRiisMgmtMonitor(){

                var riisConfigManager = window.riisConfigManager = new window.RiisConfigManager();

                var $doc = $(document);

                // add event handlers
                $doc.on('click', '#add_riis_user_btn', function(){
                    riisConfigManager.addUser.apply(riisConfigManager);
                });

                $doc.on('click', '#set_riis_threshold', function(){
                    riisConfigManager.setThreshold.apply(riisConfigManager);
                });

                $doc.on('click', '#view_login_records', function(){
                    riisConfigManager.loadLoginRecords.apply(riisConfigManager);
                });

            }

            YAHOO.util.Event.onDOMReady(addRiisMgmtMonitor);

        })();

    </script>

</pg:restricted>
