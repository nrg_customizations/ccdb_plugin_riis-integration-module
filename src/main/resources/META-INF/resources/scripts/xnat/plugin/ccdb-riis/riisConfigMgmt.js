;

console.log('riisConfigMgmt.js');

(function(){

    // RiisConfigManager()
    // Encapsulates operations required for retrieving and updating the
    // RIIS Configuration on the server as well as updating the Configuration GUI.
    function RiisConfigManager(){

        this.xnatUserLst = [];   //Array to store valid XNAT Users.

        //REST URL to retrieve the RIIS configuration.
        this.riisConfigUrl = serverRoot + '/REST/config/riisConfig/config';
        this.xnatUsersUrl = serverRoot + '/REST/users?format=json';
        this.loginRecordsUrl = serverRoot + '/data/services/riis?count=100&format=json';

        //Add the RIIS user table.
        this.riis_mgmt_div = document.getElementById("riis_mgmt_div");
        this.riis_table_div = document.createElement("div");
        this.riis_table_div.id = "riis_user_table";
        this.riis_mgmt_div.appendChild(this.riis_table_div);

        //Add the RIIS threshold Display div.
        this.riis_threshold_display = document.createElement("div");
        this.riis_threshold_display.id = "riis_threshold_display";
        this.riis_mgmt_div.appendChild(this.riis_threshold_display);

        //Add the RIIS critical error message div
        this.riis_critical_err_div = document.getElementById("riis_critical_err_holder");

        //Get the User and Threshold Fields for later use.
        this.add_user_field = document.getElementById('riisUser');
        this.set_threshold_field = document.getElementById('riisThreshold');

        //Get error elements for later use.
        this.add_user_error = document.getElementById('userAddError');
        this.set_threshold_error = document.getElementById('thresholdError');

        //Get the RIIS login Records div and button for later use.
        this.login_records_div = document.getElementById("riis_login_records");
        this.login_records_btn = document.getElementById("view_login_records");

        //Login Records div is hidden by default.
        this.loginRecordsVisible = false;

        //Takes the value in the threshold field and adds it to the configuration,
        //then pushes the new configuration to the server.
        this.setThreshold = function(){
            this.getRiisConfiguration(function(o){

                //Get and Validate the new threshold defined by the user.
                var threshold = this.set_threshold_field.value;
                if (this.isThresholdValid(threshold)) {

                    //Extract the configuration from the response text.
                    var configObj = this.getConfigObject(o.responseText);

                    //Update the config threshold and PUT the new config to the server
                    configObj.Threshold = parseInt(threshold);
                    this.putRiisConfiguration(configObj);
                }
            });
        };

        //Takes the value in the user field and adds it to the configuration,
        //then pushes the new configuration to the server.
        this.addUser = function(){
            this.getRiisConfiguration(function(o){

                //Extract the configuration from the response text.
                var configObj = this.getConfigObject(o.responseText);

                //Get and validate the username defined by the user
                var user = this.add_user_field.value;

                if (this.isUserValid(user, configObj.Users)) {

                    //Push the new username on the array of users in the configuration.
                    configObj.Users.push(user);

                    //Put the new configuration to the server.
                    this.putRiisConfiguration(configObj);
                }
            });
        };

        //Retrieves the current configuration from the server, Removes the user from the
        //configuration and then pushes the new configuration to the server.
        this.removeUser = function(user){
            this.getRiisConfiguration(function(o){

                // Extract the configuration from the response text.
                var configObj = this.getConfigObject(o.responseText);

                // Get the array index of the username in the configuration Users array.
                var i = this.getArrayIndexofValue(configObj.Users, user);

                if (i >= 0) { // If the user is in the array
                    // Remove the user from the array and PUT the new config to the server.
                    configObj.Users.splice(i, 1);
                    this.putRiisConfiguration(configObj);
                }
            });
        };

        //Function makes asyncRequest to put the given configuration to the server.
        //Takes one parameter, the config to put.
        this.putRiisConfiguration = function(config){
            this.insertCallback = {
                success: this.putSuccess,
                failure: this.putFailed,
                scope: this
            };
            YAHOO.util.Connect.initHeader("Content-Type", "text/plain");
            YAHOO.util.Connect.asyncRequest('PUT', this.riisConfigUrl + '?inbody=true&XNAT_CSRF=' + csrfToken, this.insertCallback, YAHOO.lang.JSON.stringify(config), this);
        };

        //Function gets the current RIIS configuration from the server.
        //Takes one parameter, the callback function to call on success.
        this.getRiisConfiguration = function(success){
            var getUrl = this.riisConfigUrl + '?format=json';
            this.insertCallback = {
                failure: this.getRIISConfigFailed,
                success: success,
                scope: this
            };
            YAHOO.util.Connect.asyncRequest('GET', getUrl, this.insertCallback, null, this);
        };

        //Alerts the user that something went wrong while pushing the new configuration
        //to the server.
        this.putFailed = function(o){
            if (o.status == 401) {
                this.handle401;
            }
            else {
                alert("ERROR " + o.status + ": Failed to push new configuration to the server.\n");
            }
        };

        //Called when we successfully put a new configuration to the server.
        this.putSuccess = function(){
            //Render the new data.
            this.render();
        };

        //Updates the display if we failed to retrieve a list of valid XNAT users.
        this.getXNATUserLstFailed = function(o){
            //Something is terribly wrong if we can't retrieve a list of users.
            this.addCriticalErrorMsg("ERROR " + o.status + ": Unable to retrieve list of valid XNAT users from the server.", "riis_err1");
            this.setFormDisabled(true);
            if (o.status == 401) { this.handle401; }
        };

        //Updates the display if we failed to retrieve the RIIS configuration.
        this.getRIISConfigFailed = function(o){
            //Something is terribly wrong if we can't retrieve the RIIS configuration.
            this.addCriticalErrorMsg("ERROR " + o.status + ": Unable to retrieve RIIS Configuration from the server.", "riis_err2");
            this.setFormDisabled(true);
            if (o.status == 401) { this.handle401; }
        };

        //Alerts the user if we failed to retrieve a list of login records.
        this.getLoginRecordsFailed = function(o){
            if (o.status == 401) {
                this.handle401;
            }
            else {
                alert("ERROR " + o.status + ": Could not load Riis Login Records.");
            }
        };

        //This is a special message in case of HTTP status 401.
        this.handle401 = function(){
            alert("WARNING: Your session has expired.  You will need to re-login and navigate to the content.");
            window.location = serverRoot + "/app/template/Login.vm";
        };

        //Extracts the configuration contents from the JSON Response text sent
        //from the server.
        this.getConfigObject = function(jsonTxt){
            var config = YAHOO.lang.JSON.parse(jsonTxt);
            return YAHOO.lang.JSON.parse(config.ResultSet.Result[0].contents);
        };

        //Determines if the given threshold is valid.  If it is not valid
        //the function displays an error message.
        this.isThresholdValid = function(threshold){
            if (undefined == threshold || threshold == '') { // undefined or empty.
                this.addErrorMsg(this.set_threshold_error, "Threshold must not be empty.");
                return false;
            }
            else if (threshold <= 0) { // less or equal to zero.
                this.addErrorMsg(this.set_threshold_error, "Threshold must be greater than zero.");
                return false;
            }
            else if (!threshold.match(/^\d+$/)) { // not a digit.
                this.addErrorMsg(this.set_threshold_error, "Threshold must be a numeric value.");
                return false;
            }

            //If we get to this point then the threshold is valid,
            //so remove the error (if there is one) and return true.
            this.set_threshold_error.className = "riisHidden riisError";
            return true;
        };

        //Determines if the given user is valid.  If not valid the function
        //displays an error message. users is the list of valid RIIS users.
        this.isUserValid = function(user, users){
            if (undefined === user || user === "") { //undefined or empty.
                this.addErrorMsg(this.add_user_error, "User must not be blank.");
                return false;
            }
            else if (this.getArrayIndexofValue(this.xnatUserLst, user) < 0) { //Not in the list of valid XNAT users.
                this.addErrorMsg(this.add_user_error, user + " is not a valid XNAT user.");
                return false;
            }
            else if (this.getArrayIndexofValue(users, user) >= 0) { // Already added to the configuration.
                this.addErrorMsg(this.add_user_error, user + " has already been added to the RIIS users list.");
                return false;
            }

            //If we get to this point then the user is valid,
            //so remove the error (if there is one) and return true.
            this.add_user_error.className = "riisHidden riisError";
            return true;
        };

        //Function Updates the given Error element (e) with
        //the given error message. (msg)
        this.addErrorMsg = function(e, msg){
            e.innerHTML = msg;
            e.className = "riisError";
        };

        //Function adds a new critical error message to the page
        this.addCriticalErrorMsg = function(msg, id){

            //Make sure error message with the given id doesn't already exist
            if (!document.getElementById(id)) {

                //Create a new error holder div
                var new_err = document.createElement("div");
                new_err.id = i;
                new_err.className = "riis_critical_error error";

                //Create a div for the actual message
                var msg_div = document.createElement("div");
                msg_div.innerHTML = msg;

                //Add the new error to the screen.
                new_err.appendChild(msg_div);
                this.riis_critical_err_div.appendChild(new_err);
                this.riis_critical_err_div.className = "";
            }
        };

        //Function sets the input fields to disabled.
        //Except the view login records button.
        this.setFormDisabled = function(value){
            var inputs = riisConfig.getElementsByTagName("input");
            for (var i in inputs) {
                if (inputs[i].id != "view_login_records") {
                    inputs[i].disabled = value;
                }
            }
        };

        //Returns the array index of the given value.
        //If the value is not in the array the function returns -1.
        this.getArrayIndexofValue = function(a, v){

            //Sanity Check. Array and value are not undefined or empty.
            if (a === undefined || a.length === 0 || v === undefined || v === "") { return -1; }

            //Search the array for the given value; return the index if we find it.
            for (var i in a) { if (a[i] === v) { return i; } }

            //Return -1 if we find nothing.
            return -1;
        };

        //Function retrieves a list of registered users for this instance of
        //XNAT.
        this.getXNATUserLst = function(){
            this.insertCallback = {
                scope: this,
                failure: this.getXNATUserLstFailed,
                success: function(o){

                    //Parse the JSON text returned by the server.
                    var resultObj = YAHOO.lang.JSON.parse(o.responseText);

                    //For each user in the user list returned.
                    for (var user in resultObj.ResultSet.Result) {
                        //Push the user login IDs into the xnatUserLst Array.
                        this.xnatUserLst.push(resultObj.ResultSet.Result[user]["login"]);
                    }
                }
            };

            YAHOO.util.Connect.asyncRequest('GET', this.xnatUsersUrl, this.insertCallback, null, this);
        };

        //Function retrieves the latest configuration from the server and
        //Renders the user table and threshold display.
        this.render = function(){
            this.getRiisConfiguration(function(o){

                //parse the JSON text returned by the server.
                var configObj = this.getConfigObject(o.responseText);

                //Show last user added first.
                configObj.Users.reverse();

                //Create a new data set with the JSON
                var data = new YAHOO.util.DataSource(configObj.Users);
                data.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;

                //Define response schema.
                data.responseSchema = { fields: ["userName"] };

                //Define the formatter for the remove user button.
                var removeButtonFormatter = function(elCell, oRecord, oColumn, oData){
                    elCell.innerHTML = "<input type=\"button\" ONCLICK=\"window.riisConfigManager.removeUser('" +
                        oRecord.getData("userName") + "');\" value=\"Remove\"/>";
                };

                //Define column headers
                var columns = [
                    { key: "userName", label: "User Name" },
                    { key: "removeUser", label: "Remove", formatter: removeButtonFormatter }
                ];

                //If more than 6 users, make list scrollable.
                if (configObj.Users.length > 5) { var config = { scrollable: true, height: "150" }; }

                //Create the new dataTable using the data set created above.
                var t = new YAHOO.widget.DataTable("riis_user_table", columns, data, config);

                //Display the threshold
                this.riis_threshold_display.innerHTML = "Threshold: " + configObj.Threshold + " minutes";
            });
        };

        //Function loads the list of login records and displays them in a
        //YUI Data Table and updates the GUI accordingly.
        this.loadLoginRecords = function(){

            if (true === this.loginRecordsVisible) {
                //If the loginRecords are currently visible, hide them.
                this.showLoginRecordsDiv(false);
            }
            else {
                this.getLoginRecords(function(o){

                    if (204 === o.status) {
                        // Hacked this in here so that something is displayed when there is nothing to display.
                        this.login_records_div.innerHTML = '<p style="text-align:center">No login records to display at this time :(</p>';
                    }
                    else {
                        this.login_records_div.innerHTML = "";
                    }

                    //Show the loginRecords div
                    this.showLoginRecordsDiv(true);

                    //parse the JSON text returned by the server.
                    var recObj = YAHOO.lang.JSON.parse(o.responseText);

                    //Create a new data set with the JSON
                    var data = new YAHOO.util.DataSource(recObj.ResultSet.Result);
                    data.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;

                    //Define response Schema
                    data.responseSchema = { fields: ["Project ID", "Login Time", "Ae Title"] };

                    //Define column headers
                    var columns = [
                        { key: "Project ID", label: "Project ID", width: 200, sortable: true },
                        { key: "Login Time", label: "Login Time", width: 200, sortable: true },
                        { key: "Ae Title", label: "AE Title", width: 200, sortable: true }
                    ];

                    //If more than 6 users, make list scrollable.
                    if (recObj.ResultSet.Result.length > 15) { var config = { scrollable: true, height: "350" }; }

                    //Create the new dataTable using the data set created above.
                    var t = new YAHOO.widget.DataTable("riis_login_records", columns, data, config);
                });
            }
        };

        //Function makes an asyncRequest to the server and retrieves the RIIS login records
        //as JSON. Takes one parameter, the callback function to call on success.
        this.getLoginRecords = function(success){
            this.insertCallback = {
                success: success,
                failure: this.getLoginRecordsFailed,
                scope: this
            };
            YAHOO.util.Connect.asyncRequest('GET', this.loginRecordsUrl, this.insertCallback, null, this);
        };

        //Function handles updating the gui every time the show/hide login records
        //button is pressed.
        this.showLoginRecordsDiv = function(show){
            if (true === show) {
                this.login_records_div.className = "";
                this.loginRecordsVisible = true;
                this.login_records_btn.value = "Hide Recent Login Records";
            }
            else {
                this.login_records_div.className = "riisHidden";
                this.loginRecordsVisible = false;
                this.login_records_btn.value = "View Recent Login Records";
            }
        };

        this.render();           //Render the user table and threshold display
        this.getXNATUserLst();   //Get the list of XNAT users.
    }

    window.RiisConfigManager = RiisConfigManager;

})();
