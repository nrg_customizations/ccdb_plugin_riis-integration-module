package org.nrg.xnat.riis.cache;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;
import org.nrg.xnat.riis.entities.RiisLoginRecord;

/**
 * Class for holding a RiisLoginRecord along with its associated 
 * DICOM Study Date/Time. 
 */
@Data
@Accessors(prefix = "_")
public final class CachedRiisLoginRecord{
   /**
    * Constructor. Sets the date and login record we wish to hold. 
    * @param date The DICOM study date string associated with the RiisLoginRecord
    * @param loginRecord The RiisLoginRecord we want to store.
    */
   public CachedRiisLoginRecord(final Date date, final RiisLoginRecord loginRecord){
      _date        = new Date(date.getTime());
      _loginRecord = loginRecord;
   }
   
   private final Date             _date;
   private final RiisLoginRecord  _loginRecord;
}