package org.nrg.xnat.riis.cache;

import org.nrg.xnat.riis.entities.RiisLoginRecord;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class acts as a pseudo cache to store RiisloginRecords for quick retrieval
 * without having to ask the database over and over (e.x uploading a DICOM session with 10k files.)
 * Each RiisLoginRecord is stored as a CachedRiisLoginRecord Object (RiisLoginRecord + Dicom
 * Study Date/Time) and then inserted into a HashMap (AeTitle is the key).
 */
public final class RiisServiceCacheManager {
    /**
     * Returns the singleton instance of this class.
     *
     * @return RiisServiceCacheManager object.
     */
    public static RiisServiceCacheManager getInstance() {
        return InstanceHolder.instance;
    }

    /**
     * Caches the RiisLoginRecord along with the date and aeTitle associated
     * with it. If there is already a CachedRiisLogin Record for the given aeTitle. This
     * method will overwrite what is already there with the new data.
     *
     * @param aeTitle     The aeTitle this loginRecord is associated with.
     * @param date        The date that matches this loginRecord
     * @param loginRecord The riisLoginRecord we want to save
     */
    public void cacheLoginRecord(final String aeTitle, final Date date, final RiisLoginRecord loginRecord) {
        // Create a new CachedLoginRecord and store it in the hashMap.
        _cachedLoginRecords.put(aeTitle, new CachedRiisLoginRecord(date, loginRecord));
    }

    /**
     * Retrieves a RiisLoginRecord from the cache.
     *
     * @param aeTitle The aeTitle the loginRecord is associated with.
     * @param date    The date that matches the loginRecord
     *
     * @return RiisLoginRecord object
     */
    public RiisLoginRecord getLoginRecord(final String aeTitle, final Date date) {
        //Retrieve the CachedRiisLoginRecord from the hashMap
        final CachedRiisLoginRecord cachedLoginRecord = _cachedLoginRecords.get(aeTitle);

        // Return the RiisLoginRecord if the CachedRiisLoginRecord is not null and if the dates match
        return null != cachedLoginRecord && datesAreEqual(date, cachedLoginRecord.getDate()) ? cachedLoginRecord.getLoginRecord() : null;
    }

    /**
     * Determines if there is a LoginRecord cached for the given aeTitle and date.
     *
     * @param aeTitle The aeTitle the loginRecord is associated with.
     * @param date    The date that matches the loginRecord
     *
     * @return true if there is a match, otherwise false
     */
    public boolean isLoginRecordCached(final String aeTitle, final Date date) {
        //Retrieve the CachedRiisLoginRecord from the hashMap
        //Returns null if it there is nothing cached.
        final CachedRiisLoginRecord cachedLoginRecord = _cachedLoginRecords.get(aeTitle);

        //Return true if the CachedLoginRecord is not null and the dates match.
        return null != cachedLoginRecord && datesAreEqual(date, cachedLoginRecord.getDate());
    }

    /**
     * Convenience function to determine if two date objects are equal.
     * Just makes the above code easier to read.
     *
     * @param d1 Date to compare
     * @param d2 Date to compare
     *
     * @return - true if the date objects are equal.
     */
    public boolean datesAreEqual(final Date d1, final Date d2) {
        return (0 == d1.compareTo(d2));
    }

    //Private Constructor
    private RiisServiceCacheManager() {
    }

    //Inner Class Instance Holder
    private static class InstanceHolder {
        private static final RiisServiceCacheManager instance = new RiisServiceCacheManager();
    }

    //HashMap to store CachedRiisLoginRecords.
    private final Map<String, CachedRiisLoginRecord> _cachedLoginRecords = new HashMap<>();
}