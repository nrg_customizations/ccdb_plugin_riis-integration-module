package org.nrg.xnat.riis.daos;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnat.riis.entities.RiisLoginRecord;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

@Repository
public final class RiisLoginRecordDAO extends AbstractHibernateDAO<RiisLoginRecord> {
    /**
     * Returns a list of all LoginRecords stored in the database.
     * List is ordered by descending loginTime.
     *
     * @return list of all RiisLoginRecord objects. Null size == 0;
     */
    @SuppressWarnings("unused")
    @Nullable
    public List<RiisLoginRecord> getAll() {
        return findRecordsByCriteria(null, null, null, null, -1);
    }

    /**
     * Returns a list of all LoginRecords stored in the database.
     * List is ordered by descending loginTime.
     *
     * @param maxResults The maximum number of records to return.
     *
     * @return list of all RiisLoginRecord objects. Null size == 0;
     */
    public List<RiisLoginRecord> getAll(final int maxResults) {
        return findRecordsByCriteria(null, null, null, null, maxResults);
    }

    /**
     * Returns a list of all LoginRecords with the given aeTitle.
     * List is ordered by descending loginTime.
     *
     * @param aeTitle    The aeTitle that we want to check for.
     * @param maxResults The maximum number of records to return.
     *
     * @return list of all RiisLoginRecord objects with the aeTitle. Null if size == 0.
     */
    public List<RiisLoginRecord> findRecordsByAeTitle(final String aeTitle, final int maxResults) {
        return findRecordsByCriteria(null, aeTitle, null, null, maxResults);
    }

    /**
     * Returns a list of all LoginRecords for the given project.
     * List is ordered by descending loginTime.
     *
     * @param projectId  The project that we want to check for.
     * @param maxResults the maximum number of records to return.
     *
     * @return list of all RiisLoginRecord objects wih the project. Null if size == 0.
     */
    public List<RiisLoginRecord> findRecordsByProjectId(final String projectId, final int maxResults) {
        return findRecordsByCriteria(projectId, null, null, null, maxResults);
    }

    /**
     * Returns a list of all LoginRecords for the given aeTitle where the
     * login time is between the two given dates. (minDate and maxDate)
     * List is ordered by descending loginTime.
     *
     * @param minDate The minimum date.
     * @param maxDate The maximum date.
     * @param aeTitle The aeTitle to filter on.
     *
     * @return loginRecords - a list of loginRecords. Null if size == 0.
     */
    public List<RiisLoginRecord> findRecordsWithinDateRange(final Date minDate, final Date maxDate, final String aeTitle) {
        return findRecordsByCriteria(null, aeTitle, minDate, maxDate, 0);
    }

    private List<RiisLoginRecord> findRecordsByCriteria(final String projectId, final String aeTitle, final Date minDate, final Date maxDate, final int maxResults) {
        //noinspection unchecked
        final Criteria criteria = getSession().createCriteria(getParameterizedType()).addOrder(Order.desc("loginTime"));
        if (StringUtils.isNotBlank(projectId)) {
            criteria.add(Restrictions.eq("projectId", projectId));
        }
        if (StringUtils.isNotBlank(aeTitle)) {
            criteria.add(Restrictions.eq("aeTitle", aeTitle));
        }
        if (minDate != null) {
            criteria.add(Restrictions.between("loginTime", minDate, ObjectUtils.defaultIfNull(maxDate, new Date())));
        }
        if (maxResults > 0) {
            criteria.setMaxResults(maxResults);
        }

        //noinspection unchecked
        final List<RiisLoginRecord> loginRecords = criteria.list();

        // If the loginRecords is size 0: return null, else return loginRecords.
        return (loginRecords == null || loginRecords.size() == 0) ? null : loginRecords;
    }
}
