package org.nrg.xnat.riis;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "ccdbRiis", name = "XNAT 1.7 RIIS module Plugin",
            entityPackages = "org.nrg.xnat.riis.entities",
            description = "This is the XNAT 1.7 RIIS module Plugin.",
            log4jPropertiesFile = "riis-module-log4j.properties")
@ComponentScan({"org.nrg.xnat.riis.services.impl", "org.nrg.xnat.riis.daos"})
public class RiisModulePlugin {
}
