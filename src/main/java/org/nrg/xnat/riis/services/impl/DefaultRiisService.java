package org.nrg.xnat.riis.services.impl;

import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnat.riis.cache.RiisServiceCacheManager;
import org.nrg.xnat.riis.daos.RiisLoginRecordDAO;
import org.nrg.xnat.riis.entities.RiisLoginRecord;
import org.nrg.xnat.riis.services.RiisService;
import org.nrg.xdat.preferences.SiteConfigPreferences;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static lombok.AccessLevel.PROTECTED;

/**
 * Implementation of RiisService.  Responsible for creating new
 * RiisLoginRecords and storing/retrieving them from the database.
 */
@Service
@Slf4j
@Getter(PROTECTED)
@Accessors(prefix = "_")
public class DefaultRiisService extends AbstractHibernateEntityService<RiisLoginRecord, RiisLoginRecordDAO> implements RiisService {
    /**
     * Class Constructor
     * Creates a default configuration if one doesn't already exist.
     */
    public DefaultRiisService(final SiteConfigPreferences preferences, final ConfigService configService) {
        log.info("Now starting the default RIIS service");
        _preferences = preferences;
        _configService = configService;
        initConfiguration();
    }

    /**
     * Creates and Stores a new Login Records using the given parameters.
     *
     * @param projectId The project ID.
     * @param loginTime The RIIS login time and date. This will be converted
     * @param aeTitle   The aeTitle the scanner will send to.
     *                  to and stored as a Java Date object.
     */
    @Transactional
    public void storeLoginRecord(final String projectId, final Date loginTime, final String aeTitle) {
        log.debug("Storing new loginRecord. Project ID: {}, login time: {}, AE title: {}", projectId, loginTime, aeTitle);
        getDao().create(new RiisLoginRecord(projectId, loginTime, aeTitle));
    }

    /**
     * Returns a list containing all Login Records.
     *
     * @param maxResults the maximum number of records to return.
     *
     * @return List of all Login Records.
     */
    @Transactional
    public List<RiisLoginRecord> getLoginRecords(final int maxResults) {
        return getDao().getAll(maxResults);
    }

    /**
     * Returns a list of all login records with the given AE title.
     *
     * @param aeTitle The aeTitle that we want to check for.
     * @param maxResults The maximum number of records to return.
     *
     * @return list of records containing the aeTitle. Null if list is null or size == 0;
     */
    @Transactional
    public List<RiisLoginRecord> getLoginRecordsForAeTitle(final String aeTitle, final int maxResults) {
        return getDao().findRecordsByAeTitle(aeTitle, maxResults);
    }

    /**
     * Returns a list of all LoginRecords for the given project
     *
     * @param projectId The projectId that we want to check for.
     * @param maxResults The maximum number of records to return.
     *
     * @return list of records containing the projectId. Null if list is null or size == 0;
     */
    @Transactional
    public List<RiisLoginRecord> getLoginRecordsForProjectId(final String projectId, final int maxResults) {
        return getDao().findRecordsByProjectId(projectId, maxResults);
    }

    /**
     * Requests a list of loginRecords from the DAO from the given aeTitle with a loginTime between
     * the date and the date minus the threshold (one hour default).
     * This function then returns the top LoginRecord on the list returned by the DAO.
     *
     * @param date The date to reference.
     * @param aeTitle The aeTitle the dicom was sent to.
     *
     * @return RiisLoginRecord - The LoginRecord with the closest LoginTime.
     */
    @Transactional
    public RiisLoginRecord getLoginRecordWithClosestDate(final Date date, final String aeTitle) {
        // Check to see if the login record is cached.
        // If not, dig the loginRecord out of the database.
        if (!getCacheManager().isLoginRecordCached(aeTitle, date)) {
            log.debug("Could not find LoginRecord in the cache. Retrieving loginRecord from the database.");

            //Request all the records between the the date and the date minus the threshold (one hour)
            final Date minDate = getDateMinusThreshold(date);
            log.debug("Calling findRecordsWithinDateRange({}, )", minDate, date);
            final List<RiisLoginRecord> recLst    = getDao().findRecordsWithinDateRange(minDate, date, aeTitle);
            final RiisLoginRecord       retRecord = (recLst == null) ? null : recLst.get(0);

            //Update the cache so we dont have to ask the database
            //if we get the same params next time we are called upon.
            getCacheManager().cacheLoginRecord(aeTitle, date, retRecord);
            return retRecord;
        } else {
            // return the login record found in the cache.
            log.debug("LoginRecord was cached. Retrieving cached loginRecord.");
            return getCacheManager().getLoginRecord(aeTitle, date);
        }
    }

    /**
     * Method determines if the userName is allowed to use the riisService.
     *
     * @param userName The user we are interested in.
     *
     * @return bool true if user is valid : false if not valid.
     */
    public boolean canUserUseRiis(final String userName) {
        try {
            //Get the contents of the Config as a JSON object.
            final JSONObject config = getJSONfromConfig();

            //Get the list of valid RIIS Users from the JSON object. Error if array is null or empty
            final JSONArray users = config.getJSONArray("Users");
            if (null == users || users.length() == 0) {
                throw new Exception("Could not read user list from config file.");
            }

            return doesJSONArrayContainValue(users, userName);
        } catch (Exception e) {
            log.error("Please check your RIIS configuration file contents.", e);
        }

        //return false if we managed to get this far.
        return false;
    }

    /**
     * Gets a Date object that is origDate minus the threshold set in the config.
     *
     * @param date The original date
     *
     * @return Date - origDate minus threshold set in config (60 minutes default)
     */
    private Date getDateMinusThreshold(final Date date) {

        final Calendar c = Calendar.getInstance();  //Retrieve calendar instance
        final int      t = getThreshold();          //Retrieve the threshold

        //Set the time and subtract the threshold
        c.setTime(date);
        c.add(Calendar.MINUTE, -t);

        return c.getTime();
    }

    /**
     * This method gets the threshold time from the configuration file.
     * If the Configuration file does not exist, is not valid json, or if the
     * Threshold in the config isn't a valid time (e.g. Threshold: applesauce)
     * Then this will return the default threshold of 60 minutes.
     *
     * @return int threshold containing threshold in minutes
     */
    private int getThreshold() {
        try {
            //Get the contents of the Config as a JSON object.
            final JSONObject config = getJSONfromConfig();

            //Extract the threshold from the JSON. Error if threshold is null or less than zero.
            final int threshold = config.getInt("Threshold");
            if (threshold < 0) {
                throw new Exception("Threshold retrieved was less than zero.");
            }

            log.debug("Successfully retrieved the threshold from config file. Threshold is: {} minutes.", threshold);
            return threshold;
        } catch (Exception e) {
            //If the data from the config is garbage then just set threshold to 60 minutes.
            log.error("Please check your RIIS configuration file contents.", e);
            log.error("Defaulting threshold to 60 minutes.");

            return 60;
        }
    }

    /**
     * Method gets the contents of the configuration file and parses it into
     * a new json object.
     *
     * @return The json object created from the configuration contents.
     *
     * @throws Exception When an error occurs.
     */
    private JSONObject getJSONfromConfig() throws Exception {

        //Get the config contents as a json string.
        final String jsonStr = getConfigService().getConfigContents(_riisConfig, _riisConfigFile);

        //If the Json string is null or empty bail out.
        if (null == jsonStr || jsonStr.isEmpty()) {
            throw new Exception("Could not read RIIS Configuration.");
        }

        //Use the string to create a new json object.
        return new JSONObject(jsonStr);
    }

    /**
     * Method loops throw a JSON array and determine if it contains a value.
     *
     * @param a The JSONarray
     * @param v The value
     *
     * @return bool true the array contains the value, false otherwise.
     */
    private boolean doesJSONArrayContainValue(final JSONArray a, final String v) throws Exception {

        //Sanity check.
        if (null == a || null == v || v.isEmpty()) {
            throw new Exception("doesJSONArrayContainValue() Json Array and String must not be null/empty.");
        }

        //Loop through the array and return true if we find the value
        for (int i = 0; i < a.length(); i++) {
            if (!a.isNull(i)) {
                try { //Try to read the value as a string.
                    if (a.getString(i).equals(v)) {
                        //We found the value, return true.
                        return true;
                    }
                } catch (Exception e) {
                    log.error("Could not read JSON array value as a String", e);
                }
            }
        }
        //we found nothing.
        return false;
    }

    /**
     * Function will try to initialize the riis configuration by calling
     * createDefaultConfig().
     */
    private void initConfiguration() {
        try {
            //Attempt to create a default RIIS Configuration.
            createDefaultConfig();
        } catch (Throwable t) {
            // Something failed when creating the default configuration.
            log.error("RIIS Configuration may not be initialized.", t);
        }
    }

    /**
     * Function creates a default configuration for the RIIS service if one
     * does not already exist.
     *
     * @throws Throwable - If _configService is null or we can't retrieve the admin user.
     */
    private void createDefaultConfig() throws Throwable {
        //Look for the RIIS Configuration.  If we dont find one, create a new one.
        if (null == getConfigService().getConfigContents(_riisConfig, _riisConfigFile)) {

            //Default config contents.  Threshold set to 60 minutes and admin is the only users.
            final String defaultConfig = "{\"Threshold\":60,\"Users\":[\"admin\"]}";

            //Attempt to retrieve the site administrator user.
            final String adminUser = getAdminUser();
            if (null == adminUser) {
                throw new Exception("Site administrator not found.");
            }

            //Send the default configuration to the config service.
            getConfigService().replaceConfig(adminUser, "init", _riisConfig, _riisConfigFile, defaultConfig);
            log.debug("Successfully created default RIIS Configuration. Threshold: 60 minutes, Users: admin.");
        } else {
            //Riis Configuration exists. So dont do anything.
            log.debug("Found RIIS Config. Skipping default initialization.");
        }
    }

    private String getAdminUser() {
        return getPreferences().getPrimaryAdminUsername();
    }

    //Configuration and config file to use for retrieving
    //The RIIS configuration
    private static final String _riisConfig     = "riisConfig";
    private static final String _riisConfigFile = "config";

    //Pseudo cache so if someone uploads a DICOM session with
    //10K files, we wont ask the database for the same login Record 10K times.
    private final RiisServiceCacheManager _cacheManager = RiisServiceCacheManager.getInstance();

    private final SiteConfigPreferences _preferences;
    private final ConfigService         _configService;
}