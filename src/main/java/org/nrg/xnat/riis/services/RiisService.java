package org.nrg.xnat.riis.services;

import org.nrg.xnat.riis.entities.RiisLoginRecord;

import java.util.List;
import java.util.Date;

/**
 * RiisService interface.  All methods implemented in DefaultRiisService.java
 */
public interface RiisService{
   
   void storeLoginRecord(final String projectId, final Date loginTime, final String aeTitle);
   
   List <RiisLoginRecord> getLoginRecords(final int maxResults);
   
   List <RiisLoginRecord> getLoginRecordsForAeTitle(final String aeTitle, final int maxResults);
      
   List <RiisLoginRecord> getLoginRecordsForProjectId(final String projectId, final int maxResults);
   
   RiisLoginRecord getLoginRecordWithClosestDate(final Date dicomDate, final String aeTitle);
   
   boolean canUserUseRiis(final String userName);
}
